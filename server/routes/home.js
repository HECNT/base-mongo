var express = require('express');
var router = express.Router();
var Home = require('../controllers/home');

// METHOD'S
// GET
router.get('/test', home);
router.get('/', getAll);

// POST
router.post('/new-user', newUser);
router.post('/update-user', updateUser);

function home (req, res) {
  Home.test()
  .then(function(result){
    res.json(result);
  });
}

function getAll (req, res) {
  Home.getAll()
  .then(function(result){
    res.json(result);
  });
}

function newUser (req, res) {
  var d = {
      nombre : 'TEST',
      ap : 'Juan',
      am : 'SanAndreas',
      deporte : 'eSport',
      edad : 18
  };
  Home.newUser(d)
  .then(function(result){
    res.json(result);
  });
}

function updateUser (req, res) {
  var d = {
    edad : 999
  };
  Home.updateUser(d)
  .then(function(result){
    res.json(result);
  });
}

module.exports = router;
