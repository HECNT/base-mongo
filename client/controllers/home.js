require('../services/home');


angular.module(MODULE_NAME)
.controller('homeCtrl', ['$scope', 'HomeService', '$timeout', function($scope, HomeService, $timeout) {
  var ctrl = this;
  $scope.hola = "new life"

  $scope.init = function () {
    // GET DATA TO TABLE
    $scope.newLife();
    $scope.m_usuario = {
      nombre : null,
      ap : null,
      am : null,
      edad : null,
      deporte : null
    };
  }

  $scope.newLife = function () {
    HomeService.getTodo()
    .success(function(result){
      $scope.s_usuario = result;
    });
  }

  $scope.btnEdit = function (item) {
    $scope.g_id      = item._id;
    $scope.g_nombre          = item.nombre;
    $scope.m_usuario.nombre  = item.nombre;
    $scope.m_usuario.ap      = item.ap;
    $scope.m_usuario.am      = item.am;
    $scope.m_usuario.deporte = item.deporte;
    $scope.m_usuario.edad    = item.edad;
    $(document).ready(function(){
      // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
      $('.modal').modal();
    });
  }

  $scope.btnDoEdit = function (item) {
    console.log(item,'asdsadas');
  }

}]);
