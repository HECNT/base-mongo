// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('HomeService', ['$http', function($http) {
  var url = "http://localhost:4000";
  var urlBase = url + '/home';

  this.getTodo = function () {
    return $http.get(urlBase);
  };

}]);
